package com.egs.bank.repository;

import com.egs.bank.entity.UserKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserKeyRepository extends JpaRepository<UserKey, Long> {

    UserKey findByUserId(Long userId);

    UserKey save(UserKey userKey);

}
