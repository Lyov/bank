package com.egs.bank.repository;

import com.egs.bank.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "SELECT * from accounts a inner join transactions t on t.account_id=a.id where a.user_id =:id", nativeQuery = true)
    List<Transaction> findTransactionsByUserId(@Param("id") long id);

    @Query(value = "SELECT * from accounts a inner join transactions t on t.account_id=a.id where a.user_id =:id and t.created_at =:created_at", nativeQuery = true)
    List<Transaction> findTransactionsByUserIdAndByDate(@Param("id") long id, @Param("created_at") Date created_at);

}
