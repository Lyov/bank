package com.egs.bank.controller;


import com.egs.bank.entity.User;
import com.egs.bank.entity.UserKey;
import com.egs.bank.enumeration.Role;
import com.egs.bank.exception.BadRequest;
import com.egs.bank.exception.Conflict;
import com.egs.bank.security.config.PwEncoderConfig;
import com.egs.bank.service.JsonWebTokenService;
import com.egs.bank.service.UserService;
import com.egs.bank.util.Util;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("api/auth")
public class AuthenticationController {

    private final UserService userService;
    private final JsonWebTokenService jwtService;
    private final PwEncoderConfig pwEncoderConfig;

    public AuthenticationController(UserService userService, JsonWebTokenService jwtService, PwEncoderConfig pwEncoderConfig) {
        this.userService = userService;
        this.jwtService = jwtService;
        this.pwEncoderConfig = pwEncoderConfig;
    }

    @PostMapping(value = "/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, String> createUser(@RequestBody @Validated User user, BindingResult bdResult) {
        if (bdResult.hasErrors()) {
            throw new BadRequest();
        }

        if (this.userService.getUserByEmail(user.getEmail()) != null) {
            throw new Conflict();
        }

        String userUuid = UUID.randomUUID().toString();

        user.setPassword(this.pwEncoderConfig.passwordEncoder().encode(user.getPassword()));
        user.setUuid(userUuid);

        Long userId = this.userService.generateUser(user).getId();

        UserKey userKey = new UserKey();
        userKey.setRole(Role.ROLE_USER);
        userKey.setTokenType("Bearer");
        userKey.setToken(this.jwtService.generateJwt(userId, 365, userUuid));
        userKey.setRefreshToken(this.jwtService.generateJwt(userId, 365, userUuid));
        userKey.setExpiresAt(Util.calculateDate());
        userKey.setUserId(userId);

        this.userService.generateUserKey(userKey);
        return Collections.singletonMap("message", "ok.");
    }

    @PostMapping(value = "/login")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, UserKey> issueToken(@RequestBody @Validated User body, BindingResult bdResult) {

        if (bdResult.hasErrors()) {
            throw new BadRequest();
        }

        User user = this.userService.getUserByEmail(body.getEmail());
        if (user == null) {
            throw new BadRequest();
        }

        if (!this.pwEncoderConfig.passwordEncoder().matches(body.getPassword(), user.getPassword())) {
            throw new BadRequest();
        }

        UserKey userKey = this.userService.getUserKey(user.getId());
        return Collections.singletonMap("key", userKey);
    }

}
