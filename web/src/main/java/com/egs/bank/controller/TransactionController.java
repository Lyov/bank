package com.egs.bank.controller;


import com.egs.bank.entity.Transaction;
import com.egs.bank.exception.BadRequest;
import com.egs.bank.service.TransactionService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, String> createTransaction(@RequestBody @Validated Transaction transaction, BindingResult bdResult) {
        if (bdResult.hasErrors()) {
            throw new BadRequest();
        }
        transactionService.generateTransaction(transaction);
        return Collections.singletonMap("message", "ok.");
    }

    @PutMapping("/{id}/activation")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, String> acceptTransaction(@PathVariable(value = "id") Long id) {
        transactionService.changeTransactionStatus(id, true);
        return Collections.singletonMap("message", "ok.");
    }

    @PutMapping("/{id}/deactivation")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, String> rejectTransaction(@PathVariable(value = "id") Long id) {
        transactionService.changeTransactionStatus(id, false);
        return Collections.singletonMap("message", "ok.");
    }
}
