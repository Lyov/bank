package com.egs.bank.controller;

import com.egs.bank.entity.Transaction;
import com.egs.bank.entity.UserKey;
import com.egs.bank.exception.BadRequest;
import com.egs.bank.service.TransactionService;
import com.egs.bank.service.UserService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    private final TransactionService transactionService;

    public UserController(UserService userService, TransactionService transactionService) {
        this.userService = userService;
        this.transactionService = transactionService;
    }

    @GetMapping(value = "/{id}/transactions")
    public Map<String, List<Transaction>> getUserTransactions(@PathVariable(value = "id") Long id) {
        List<Transaction> transactions = transactionService.getUserTransactions(id);
        return Collections.singletonMap("message", transactions);
    }

    @GetMapping(value = "/{id}/transactions/{date}")
    public  Map<String, List<Transaction>>  getUserTransactionsByDate(@PathVariable(value = "id") Long id,
    @PathVariable(value="date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date createdDate) {
        List<Transaction> transactions = transactionService.getUserTransactionsByDate(id, createdDate);
        return  Collections.singletonMap("message", transactions);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}/roles")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, String> changeUserRole(@PathVariable(value = "id") Long id) {
        UserKey userKey = userService.getUserKey(id);
        if (userKey == null) {
            throw new BadRequest();
        } else {
            userService.updateUserRole(userKey);
        }
        return Collections.singletonMap("message", "ok.");
    }
}