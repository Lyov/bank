package com.egs.bank.controller;

import com.egs.bank.entity.Account;
import com.egs.bank.exception.BadRequest;
import com.egs.bank.service.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, String> createAccount(@RequestBody @Validated Account account, BindingResult bdResult) {
        if (bdResult.hasErrors()) {
            throw new BadRequest();
        }
        accountService.generateAccount(account);
        return Collections.singletonMap("message", "ok.");
    }
}
