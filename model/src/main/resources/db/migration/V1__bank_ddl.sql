CREATE TABLE IF NOT EXISTS `users` (
    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `uuid` varchar(255),
    `username` varchar(255),
    `email` varchar(255),
    `password` varchar(255),
    `created_at` DATETIME
);

CREATE TABLE IF NOT EXISTS `user_keys` (
    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `role` VARCHAR(255),
    `token_type` VARCHAR(255),
    `token` VARCHAR(500),
    `refresh_token` VARCHAR(500),
    `user_id` BIGINT,
    `expires_at` DATETIME,
    `created_at` DATETIME
);

CREATE TABLE IF NOT EXISTS `accounts` (
    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `currency` VARCHAR(255),
    `balance` DECIMAL,
    `user_id` BIGINT,
    `created_at` DATETIME,
    CONSTRAINT fk_users_accounts FOREIGN KEY (user_id)
    REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS `transactions` (
    `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `status` VARCHAR(50) DEFAULT 'PENDING',
    `type` VARCHAR(50),
    `amount` DECIMAL,
    `account_id` BIGINT,
    `created_at` DATETIME,
    CONSTRAINT fk_account_transactions FOREIGN KEY (account_id)
    REFERENCES accounts(id)
);

