package com.egs.bank.enumeration;

public enum TransactionStatus {
    PENDING, ACCEPTED, REJECTED
}
