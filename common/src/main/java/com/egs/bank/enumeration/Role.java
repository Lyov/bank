package com.egs.bank.enumeration;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}
