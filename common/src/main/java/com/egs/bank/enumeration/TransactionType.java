package com.egs.bank.enumeration;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}
