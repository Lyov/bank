package com.egs.bank.service;

import com.egs.bank.entity.Account;
import com.egs.bank.entity.Transaction;
import com.egs.bank.enumeration.TransactionStatus;
import com.egs.bank.enumeration.TransactionType;
import com.egs.bank.exception.InternalServerError;
import com.egs.bank.repository.AccountRepository;
import com.egs.bank.repository.TransactionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;

    private final AccountRepository accountRepository;

    public TransactionService(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    public List<Transaction> getUserTransactions(Long userId) {
        List<Transaction> transactions;
        try {
            transactions = transactionRepository.findTransactionsByUserId(userId);
        } catch (Exception ex) {
            throw new InternalServerError();
        }
        return transactions;
    }

    public List<Transaction> getUserTransactionsByDate(Long userId, Date createdDate) {
        List<Transaction> transaction;
        try {
            transaction = transactionRepository.findTransactionsByUserIdAndByDate(userId, createdDate);
        } catch (Exception ex) {
            throw new InternalServerError();
        }
        return transaction;
    }

    public Transaction generateTransaction(Transaction transaction) {
        try {
            transaction = this.transactionRepository.save(transaction);
        } catch (Exception ex) {
            throw new InternalServerError();
        }
        return transaction;
    }

    @Transactional
    public void changeTransactionStatus(Long id, boolean isAccepted) {
        Transaction transaction;
        try {
            transaction = this.transactionRepository.getById(id);
            TransactionStatus status = transaction.getStatus();
            if (status.equals(TransactionStatus.PENDING)) {
                if (isAccepted) {
                    transaction.setStatus(TransactionStatus.ACCEPTED);
                } else {
                    transaction.setStatus(TransactionStatus.REJECTED);
                }
                Account account = transaction.getAccount();
                BigDecimal balance = account.getBalance();
                if (transaction.getType().equals(TransactionType.DEPOSIT)) {
                    balance = balance.add(transaction.getAmount());
                } else {
                    balance = balance.subtract(transaction.getAmount());
                }
                account.setBalance(balance);
                accountRepository.save(account);
                transactionRepository.save(transaction);
            }
        } catch (Exception ex) {
            throw new InternalServerError();
        }
    }
}
