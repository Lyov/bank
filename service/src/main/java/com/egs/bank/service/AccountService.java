package com.egs.bank.service;

import com.egs.bank.entity.Account;
import com.egs.bank.exception.InternalServerError;
import com.egs.bank.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account generateAccount(Account account) {
        try {
            account = this.accountRepository.save(account);
        } catch (Exception ex) {
            throw new InternalServerError();
        }
        return account;
    }

    public List<Account> getAccountsByUserId(Long userId) {
        List<Account> accounts;
        try {
            accounts = this.accountRepository.findByUserId(userId);
        } catch (Exception ex) {
            throw new InternalServerError();
        }
        return accounts;
    }

}
