package com.egs.bank.service;

import com.egs.bank.entity.User;
import com.egs.bank.entity.UserKey;
import com.egs.bank.enumeration.Role;
import com.egs.bank.exception.InternalServerError;
import com.egs.bank.repository.UserKeyRepository;
import com.egs.bank.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final UserKeyRepository userKeyRepository;

    public UserService(UserRepository userRepository, UserKeyRepository userKeyRepository) {
        this.userRepository = userRepository;
        this.userKeyRepository = userKeyRepository;
    }

    public User getUserByIdAndUserUuid(Long id, String userUuid) {
        User user = this.userRepository.findByIdAndUuid(id, userUuid);
        user.setUsername("");
        user.setEmail("");
        user.setPassword("");
        return user;
    }

    public UserKey updateUserRole(UserKey userKey) {
        if (userKey.getRole().equals(Role.ROLE_ADMIN)) {
            userKey.setRole(Role.ROLE_USER);
        } else {
            userKey.setRole(Role.ROLE_ADMIN);
        }
        return userKeyRepository.save(userKey);
    }

    public User getUserById(Long id) {
        User user;
        try {
            user = this.userRepository.getById(id);
        } catch (Exception ex) {
            throw new InternalServerError();
        }
        return user;
    }

    public User getUserByEmail(String email) {
        User user;
        try {
            user = this.userRepository.findByEmail(email);
        } catch (Exception ex) {
            throw new InternalServerError();
        }

        return user;
    }

    public User generateUser(User user) {
        try {
            user = this.userRepository.save(user);
        } catch (Exception ex) {
            throw new InternalServerError();
        }

        return user;
    }

    public void generateUserKey(UserKey userKey) {
        try {
            this.userKeyRepository.save(userKey);
        } catch (Exception ex) {
            throw new InternalServerError();
        }
    }

    public UserKey getUserKey(Long userId) {
        UserKey userKey;

        try {
            userKey = this.userKeyRepository.findByUserId(userId);
        } catch (Exception ex) {
            throw new InternalServerError();
        }

        return userKey;
    }
}