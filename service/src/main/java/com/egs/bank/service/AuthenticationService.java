package com.egs.bank.service;

import com.egs.bank.entity.User;
import com.egs.bank.entity.UserKey;
import com.egs.bank.enumeration.Role;
import com.egs.bank.repository.UserKeyRepository;
import com.egs.bank.security.model.Credential;
import com.egs.bank.security.role.AdminAuthority;
import com.egs.bank.security.role.UserAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

@Service
public class AuthenticationService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    @Autowired
    UserService userService;

    @Autowired
    JsonWebTokenService jsonWebTokenService;

    @Autowired
    UserKeyRepository userKeyRepository;

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        Object credentials = token.getCredentials();

        if (credentials.toString().equals("")) {
            throw new BadCredentialsException("Bad credential");
        }

        if (!this.jsonWebTokenService.verifyJwt(credentials.toString())) {
            throw new BadCredentialsException("Bad credential");
        }

        Credential credential = this.jsonWebTokenService.decodeJwt(credentials.toString());
        if (credential.getSub() == null || credential.getJti() == null) {
            throw new BadCredentialsException("Bad credential");
        }

        Long userId = credential.getSub();
        String userUuid = credential.getJti();

        User user = this.userService.getUserByIdAndUserUuid(userId, userUuid);
        if (user == null) {
            throw new UsernameNotFoundException("Not found user");
        }
        Collection<GrantedAuthority> authorities = new HashSet<>();

        UserKey userKey = userKeyRepository.findByUserId(user.getId());

        if (!Objects.isNull(userKey) && !Objects.isNull(userKey.getRole())) {
            if (userKey.getRole().equals(Role.ROLE_ADMIN)) {
                authorities.add(new AdminAuthority());
            }
        } else {
            authorities.add(new UserAuthority());
        }

        return new org.springframework.security.core.userdetails.User(userUuid, "", authorities);
    }
}
