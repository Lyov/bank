package com.egs.bank.security.role;

import com.egs.bank.enumeration.Role;
import org.springframework.security.core.GrantedAuthority;

public class UserAuthority implements GrantedAuthority {
    @Override
    public String getAuthority() {
        return Role.ROLE_USER.name();
    }
}
